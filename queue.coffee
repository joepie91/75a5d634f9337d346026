queueItems = []
queueRunning = false

addQueue = (item) ->
	queueItems.push item
	checkQueue()

checkQueue = ->
	if not queueRunning and queueItems.length > 0
		queueRunning = true
		currentItem = queueItems.shift() # Takes the first item and removes it from the queue

		Promise.try ->
			return doThingWith(currentItem)
		.then (result) ->
			# Do something with the `result` if you want, and then...
			queueRunning = false
			checkQueue()
